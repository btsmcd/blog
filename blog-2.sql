-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : dim. 29 oct. 2023 à 18:10
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--
-- Erreur de lecture de structure pour la table blog.article : #1142 - La commande &#039;SHOW&#039; est interdite à l&#039;utilisateur: &#039;&#039;@&#039;localhost&#039; sur la table `blog`.`article`
-- Erreur de lecture des données pour la table blog.article : #1064 - Erreur de syntaxe près de &#039;FROM `blog`.`article`&#039; à la ligne 1

-- --------------------------------------------------------

--
-- Structure de la table `catégorie`
--
-- Erreur de lecture de structure pour la table blog.catégorie : #1142 - La commande &#039;SHOW&#039; est interdite à l&#039;utilisateur: &#039;&#039;@&#039;localhost&#039; sur la table `blog`.`catégorie`
-- Erreur de lecture des données pour la table blog.catégorie : #1064 - Erreur de syntaxe près de &#039;FROM `blog`.`catégorie`&#039; à la ligne 1

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--
-- Erreur de lecture de structure pour la table blog.commentaires : #1142 - La commande &#039;SHOW&#039; est interdite à l&#039;utilisateur: &#039;&#039;@&#039;localhost&#039; sur la table `blog`.`commentaires`
-- Erreur de lecture des données pour la table blog.commentaires : #1064 - Erreur de syntaxe près de &#039;FROM `blog`.`commentaires`&#039; à la ligne 1

-- --------------------------------------------------------

--
-- Structure de la table `user`
--
-- Erreur de lecture de structure pour la table blog.user : #1142 - La commande &#039;SHOW&#039; est interdite à l&#039;utilisateur: &#039;&#039;@&#039;localhost&#039; sur la table `blog`.`user`
-- Erreur de lecture des données pour la table blog.user : #1064 - Erreur de syntaxe près de &#039;FROM `blog`.`user`&#039; à la ligne 1
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
