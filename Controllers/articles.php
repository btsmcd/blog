<?php

$bdd = new PDO("mysql:host=localhost;dbname=blog;charset=utf8", 'root', '');

$queryCategories = "SELECT nomcategorie FROM catégorie";
$categories = $bdd->query($queryCategories);

$selectedCategory = isset($_GET['category']) ? $_GET['category'] : 'all'; // Set the default to 'all'

if ($selectedCategory === 'all') {
    $queryArticle = "SELECT id, titre, contenu, auteur_article, DATE_FORMAT(date_creation, '%d/%m/%Y') as datea, nomcategorie FROM article INNER JOIN catégorie ON article.idcategorie= catégorie.idcategorie ORDER BY date_creation DESC LIMIT 10";
} else {
    $queryArticle = "SELECT id, titre, contenu, auteur_article, DATE_FORMAT(date_creation, '%d/%m/%Y') as datea, nomcategorie FROM article INNER JOIN catégorie ON article.idcategorie= catégorie.idcategorie WHERE nomcategorie = ? ORDER BY date_creation DESC";
}

$top10 = $bdd->prepare($queryArticle);

if ($selectedCategory !== 'all') {
    $top10->execute([$selectedCategory]);
} else {
    $top10->execute();
}
?>