<?php


session_start();
$bdd = new PDO('mysql:host=localhost;dbname=blog;charset=utf8;', 'root', '', array(PDO::ATTR_PERSISTENT => true));

if (isset($_POST['boutton'])) {
    if (!empty($_POST['email']) && !empty($_POST['nom']) && !empty($_POST['prénom']) && !empty($_POST['password']) && !empty($_POST['confirmation'])) {
        $email = htmlspecialchars($_POST['email']);
        $nom = htmlspecialchars($_POST['nom']);
        $prénom = htmlspecialchars($_POST['prénom']);
        $type = $_POST['type'];
        $password = $_POST['password'];
        $confirmation = $_POST['confirmation'];
        $alertMessages = array();
        if (strlen($password) < 12) {
            $alertMessages[] = "Le mot de passe doit contenir au moins 12 caractères.";
        }
        if (!preg_match('/[a-z]/', $password)) {
            $alertMessages[] = "Le mot de passe doit contenir au moins une lettre minuscule.";
        }
        if (!preg_match('/[A-Z]/', $password)) {
            $alertMessages[] = "Le mot de passe doit contenir au moins une lettre majuscule.";
        }
        if (!preg_match('/\d/', $password)) {
            $alertMessages[] = "Le mot de passe doit contenir au moins un chiffre.";
        }
        if (!preg_match('/[^a-zA-Z\d]/', $password)) {
            $alertMessages[] = "Le mot de passe doit contenir au moins un caractère spécial.";
        }
        if ($password !== $confirmation) {
            $alertMessages[] = "La confirmation du mot de passe ne correspond pas.";
        }
        if (!empty($alertMessages)) {
            foreach ($alertMessages as $message) {
                echo "<script> alert('$message') </script>";
            }
        } else {
            $passwordHash = password_hash($password, PASSWORD_DEFAULT);
            $insertUsers = $bdd->prepare('INSERT INTO user (email, nom, prénom, password, type) VALUES (?, ?, ?, ?, ?)');
            $insertUsers->execute(array($email, $nom, $prénom, $passwordHash, $type));
            $userId = $bdd->lastInsertId();
            $_SESSION['email'] = $email;
            $_SESSION['prénom'] = $prénom;
            $_SESSION['id'] = $userId;
            echo "<script> alert('inscription réussi !') </script>";
        }
    } else {
        echo "<script> alert('Veuillez remplir tous les champs.') </script>";
    }
}

?>