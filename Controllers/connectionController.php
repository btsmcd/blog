<?php
session_start();

include('../Models/BDD.php');

$bdd = new PDO("mysql:host=localhost;dbname=blog;charset=utf8", 'root', '');

if (isset($_POST['boutton'])) {
    if (!empty($_POST['email']) && !empty($_POST['password'])) {
        $email = htmlspecialchars($_POST['email']);
        $password = $_POST['password'];
        $getUser = $bdd->prepare('SELECT * FROM user WHERE email = ?');
        $getUser->execute(array($email));
        $user = $getUser->fetch();

        if ($user && password_verify($password, $user['password'])) {
            if ($user['type'] == "admin") {
                header('Location: ../Views/admin/indexadmin.php');
                exit();
            } else {
                $_SESSION['email'] = $email;
                $_SESSION['id'] = $user['id'];
                getUserInfo($bdd, $email, 'prénom');
                getUserInfo($bdd, $email, 'nom');
                echo "<script> alert('Mot de passe incorrect !') </script>";
                header('Location: ../Views/client/indexclient.php');
                exit();
            }
        } else {
            echo "<script> alert('Mot de passe incorrect !') </script>";
        }
    } else {
        echo "<script> alert('Veuillez remplir tous les champs...') </script>";
    }
}

function getUserInfo($bdd, $email, $x){
    $recup = $bdd->prepare("SELECT $x FROM user WHERE email = ?");
    $recup->execute(array($email));
    $utilisateur = $recup->fetch();
    $_SESSION[$x] = $utilisateur[$x];
}



?>


