<?php

$bdd = new PDO("mysql:host=localhost;dbname=blog;charset=utf8", 'root', '');

if (isset($_GET['id_article'])) {
    $id_article = $_GET['id_article'];
} else {
    echo "Erreur 500";
    exit();
}

$queryArticle = "SELECT titre, contenu, auteur_article, DATE_FORMAT(date_creation, '%d/%m/%Y') as datea FROM article WHERE id = ?"; // Utilisez le bon champ pour la condition WHERE
$articleInfo = $bdd->prepare($queryArticle);
$articleInfo->execute([$id_article]);
$article = $articleInfo->fetch();

$queryCommentaires = "SELECT commentaires.auteur, commentaires.commentaire, DATE_FORMAT(commentaires.date_commentaire,'le %d/%m/%Y à %Hh %imin %Ss') as Datec FROM commentaires WHERE commentaires.id_billet = ? ORDER BY commentaires.date_commentaire DESC ";

$commentaires = $bdd->prepare($queryCommentaires);
$commentaires->execute([$id_article]);
?>