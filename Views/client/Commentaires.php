<?php
session_start();
include('headerClient.php');
include('../head.php');
include('../../Controllers/Commentaires.php');

$bdd = new PDO("mysql:host=localhost;dbname=blog;charset=utf8", 'root', '');

if (isset($_GET['id_article'])) {
    $id_article = $_GET['id_article'];
} else {
    echo "Erreur 500";
    exit();
}

$queryArticle = "SELECT titre, contenu, auteur_article, DATE_FORMAT(date_creation, '%d/%m/%Y') as datea FROM article WHERE id = ?";
$articleInfo = $bdd->prepare($queryArticle);
$articleInfo->execute([$id_article]);
$article = $articleInfo->fetch();

$queryCommentaires = "SELECT commentaires.auteur, commentaires.commentaire, DATE_FORMAT(commentaires.date_commentaire,'le %d/%m/%Y à %Hh %imin %Ss') as Datec FROM commentaires WHERE commentaires.id_billet = ? ORDER BY commentaires.date_commentaire DESC ";
$commentaires = $bdd->prepare($queryCommentaires);
$commentaires->execute([$id_article]);

// Gestion de la soumission du formulaire de commentaire
if (isset($_POST['ajouter_commentaire'])) {
    $auteur_commentaire = $_POST['auteur_commentaire'];
    $nouveau_commentaire = $_POST['commentaire'];

    // Insérez le nouveau commentaire dans la base de données
    $insertCommentQuery = "INSERT INTO commentaires (id_billet, auteur, commentaire, date_commentaire) VALUES (?, ?, ?, ?)";
    $insertComment = $bdd->prepare($insertCommentQuery);
    $insertComment->execute([$id_article, $auteur_commentaire, $nouveau_commentaire]);

    // Actualisez la page pour afficher le nouveau commentaire
    header("Location: $_SERVER[PHP_SELF]?id_article=$id_article");
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../Asset/style.css">
</head>
<body>

<ul>
    <?php
    echo '<li>Titre : ' . $article['titre'] . '</li><br>';
    echo 'Contenu : ' . $article['contenu'] . '<br><br>';
    echo 'Auteur : ' . $article['auteur_article'] . '<br><br>';
    echo 'Date de création : ' . $article['datea'] . '<br><br>';
    ?>
</ul>

<!-- Bouton "Ajouter un commentaire" -->
<button id="afficherCommentForm">Ajouter un commentaire</button>

<!-- Formulaire pour ajouter un commentaire (initialement masqué) -->
<form id="commentForm" style="display: none;" method="POST">
    <label for="auteur_commentaire">Auteur du commentaire :</label>
    <input type="text" name="auteur_commentaire" required><br>

    <label for="commentaire">Commentaire :</label>
    <textarea name="commentaire" required></textarea><br>

    <button type="submit" name="ajouter_commentaire">Ajouter Commentaire</button>
</form>

<?php
echo '<div class="separator2"></div>';
while ($row = $commentaires->fetch()) {
    echo '<li>Commentaire de : '  . $row['auteur'] .' '. $row['Datec'] .'</li><br>';
    echo 'Commentaire : ' . $row['commentaire'] . '<br>';
    echo '<div class="separator"></div>';
}
?>

<!-- Script pour afficher le formulaire de commentaire lorsque le bouton est cliqué -->
<script>
    document.getElementById("afficherCommentForm").addEventListener("click", function() {
        document.getElementById("commentForm").style.display = "block";
    });
</script>

</body>
</html>
