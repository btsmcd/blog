<?php
include('headerClient.php');
include('../head.php');


$bdd = new PDO("mysql:host=localhost;dbname=blog;charset=utf8", 'root', '');

$queryArticleC = "SELECT id, titre, contenu, auteur_article, DATE_FORMAT(date_creation, '%d/%m/%Y') as datea, nomcategorie 
FROM user
    INNER JOIN catégorie 
        ON article.idcategorie= catégorie.idcategorie 
    INNER JOIN commentaires
        ON commentaires.id_billet=article.id
    INNER JOIN USER
    ORDER BY date_creation DESC";
$ArticlesC = $bdd->query($queryArticleC);