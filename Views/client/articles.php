<?php
session_start();
include('headerClient.php');
include('../head.php');
include('../../Controllers/articles.php');

?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../Asset/style.css">
</head>
<body>
<a href="?category=all"><button>Tous</button></a>
<?php
foreach ($categories as $categorie) {
    $nomCategorie = $categorie['nomcategorie'];
    $categoryParam = str_replace(' ', '+', $nomCategorie); // Convert spaces to + for the URL
    echo "<a href='?category=$categoryParam'><button>" . $nomCategorie . "</button></a>";
    echo "<br>";
}
?>

<ul>
    <?php
    while ($row = $top10->fetch()) {
        echo '<li>Titre : ' . $row['titre'] . '</li><br>';
        echo 'Contenu : ' . $row['contenu'] . '<br><br>';
        echo 'Auteur : ' . $row['auteur_article'] . '<br><br>';
        echo 'Catégorie : ' . $row['nomcategorie'] . '<br><br>';
        echo 'Date de création : ' . $row['datea'] . '<br><br>';
        echo '<a href="commentaires.php?id_article=' . $row['id'] . '">Commentaires</a>';
        echo '<div class="separator"></div>';
    }
    ?>
</ul>
</body>
</html>

