<?php
session_start();
include('headerAdmin.php');
include('../head.php');
include('../../Controllers/connectionController.php');

$bdd = new PDO("mysql:host=localhost;dbname=blog;charset=utf8", 'root', '');

if (isset($_GET['id_article'])) {
    $id_article = $_GET['id_article'];
} else {
    echo "Erreur 500";
    exit();
}

$queryArticle = "SELECT titre, contenu, auteur_article, DATE_FORMAT(date_creation, '%d/%m/%Y') as datea FROM article WHERE id = ?"; // Utilisez le bon champ pour la condition WHERE
$articleInfo = $bdd->prepare($queryArticle);
$articleInfo->execute([$id_article]);
$article = $articleInfo->fetch();

$queryCommentaires = "SELECT commentaires.auteur, commentaires.commentaire, DATE_FORMAT(commentaires.date_commentaire,'le %d/%m/%Y à %Hh %imin %Ss') as Datec FROM commentaires WHERE commentaires.id_billet = ? ORDER BY commentaires.date_commentaire DESC ";

$commentaires = $bdd->prepare($queryCommentaires);
$commentaires->execute([$id_article]);

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../Asset/style.css">
</head>
<body>
<ul>
    <?php
    echo '<li>Titre : ' . $article['titre'] . '</li><br>';
    echo 'Contenu : ' . $article['contenu'] . '<br><br>';
    echo 'Auteur : ' . $article['auteur_article'] . '<br><br>';
    echo 'Date de création : ' . $article['datea'] . '<br><br>';
    echo '<div class="separator2"></div>';
    while ($row = $commentaires->fetch()) {
        echo '<li>Commentaire de : '  . $row['auteur'] .' '. $row['Datec'] .'</li><br>';
        echo 'Commentaire : ' . $row['commentaire'] . '<br>';
        echo '<div class="separator"></div>';
    }
    ?>
</ul>
</body>
</html>


